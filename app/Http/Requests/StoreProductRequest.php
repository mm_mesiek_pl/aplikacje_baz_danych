<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Pole *nazwa* jest wymagane!',
            'price.required' => 'Pole *cena* jest wymagane!',
            'price.numeric' => 'Pole *cena* musi być liczbą!',
            'quantity.required' => 'Pole *ilość* jest wymagane!',
            'quantity.integer' => 'Pole *ilość* musi być liczbą!',
        ];
    }
}
