<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'amount.required' => 'Pole *ilość użytkowników* jest wymagane!',
            'amount.integer' => 'Pole *ilość użytkoników* przyjmuje tylko wartości liczbowe!',
        ];
    }
}
