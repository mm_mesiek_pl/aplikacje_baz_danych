<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'last_name' => 'required',
            'city' => 'required',
            'street' => 'required',
            'birthday' => 'required',
            'eyes' => 'required',
            'growth' => 'required|integer',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Pole *imię* jest wymagane!',
            'last_name.required' => 'Pole *nazwisko* jest wymagane!',
            'city.required' => 'Pole *miasto* jest wymagane!',
            'street.required' => 'Pole *ulica* jest wymagane!',
            'birthday.required' => 'Pole *urodziny* jest wymagane!',
            'eyes.required' => 'Pole *oczy* jest wymagane!',
            'growth.required' => 'Pole *wzrost* jest wymagane!',
            'growth.integer' => 'Pole *wzrost* musi być wartością liczbową!',
            'description.required' => 'Pole *opis* jest wymagane!',
        ];
    }
}
