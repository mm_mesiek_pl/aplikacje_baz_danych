<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\StoreDrawRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use function Symfony\Component\VarDumper\Dumper\esc;
use Exception;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_database()
    {
        Artisan::call('migrate:reset');
        return "Tabele zostały poprawnie usunięte.";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_database()
    {
        Artisan::call('migrate');
        return "Tabele zostały poprawnie utworzone.";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $columns = [
            'name' => 'Imię',
            'last_name' => 'Nazwisko',
            'city' => 'Miasto',
            'street' => 'Ulica',
            'birthday' => 'Urodziny',
            'eyes' => 'Oczy',
            'growth' => 'Wzrost',
            'description' => 'Opis',
        ];

        // z geta
        $fraza = $request->fraza;
        $kolumna = $request->kolumna;

        session(['column'=>$kolumna]);
        session(['pharse'=>$fraza]);

        $count_clients = null;
        $count_file_clients = null;

        $clients = Client::orderBy('id', 'desc')->get();


        // Normalna wyszukiwarka
        if ($request->has('fraza') && $request->has('kolumna')) {
            if ($request->has('resetuj')) {
                $clients = Client::orderBy('id', 'desc')->get();
//                Session::flush();
            } else {
                $clients = Client::orderBy('id', 'desc')->where($request->kolumna,'LIKE','%'.$request->fraza.'%')->get();
                $count_clients = $clients->count();
            }

        }


        // Wyszukiwarka z pliku
        if ($request->has('file_query')) {
            if ($request->has('resetuj_plik')) {
                $clients = Client::orderBy('id', 'desc')->get();
//                Session::flush();
            } else {

                try {
                    $file = fopen($request->file('file_query'), "r");
                    $first_line =  explode(':', fgets($file));

                    $column = $first_line[0];
                    $pharse = $first_line[1];

                    session(['column'=>$column]);
                    session(['pharse'=>$pharse]);

                    $clients = Client::orderBy('id', 'desc')->where(session('column'),'LIKE','%'.session('pharse').'%')->get();
                    $count_file_clients = $clients->count();
                } catch(\Exception $exception) {
                    return back()->with('error', 'Z Twoim plikiem jest coś nie tak... Zapoznaj się z wymaganiami pliku!');

                }

            }

        }


        return view('index', ['clients' => $clients, 'columns' => $columns, 'fraza' => $fraza, 'kolumna' => $kolumna, 'count_clients' => $count_clients, 'count_file_clients' => $count_file_clients]);
    }

    public function export_csv(Request $request) {

//        if ($request->has('exportuj')) {
            $clients = Client::orderBy('id', 'desc')->where(session('column'),'LIKE','%'.session('pharse').'%')->get();
            $csvExporter = new \Laracsv\Export();
            return $csvExporter->build($clients, ['name', 'last_name', 'city', 'street', 'birthday', 'eyes', 'growth', 'description'])->download(session('pharse') . '_export.csv');
//        }

    }

//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function create()
//    {
//
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientRequest $request)
    {
        $user = new Client($request->all());
        $user->save();
        return back()->with('status', 'Użytkownik został poprawnie dodany do bazy danych!');

    }

    /**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
    public function store_from_file(Request $request)
    {
        try{
            $file = fopen($request->file('file'), "r");


            while(!feof($file))
            {
                $line[] = explode(',', fgets($file));        }

            fclose($file);

            for($i = 0; $i <= count($line)-1; $i++) {
                $user = Client::create([
                    'name' => $line[$i][0],
                    'last_name' => $line[$i][1],
                    'city' => $line[$i][2],
                    'street' => $line[$i][3],
                    'birthday' => $line[$i][4],
                    'eyes' => $line[$i][5],
                    'growth' => $line[$i][6],
                    'description' => $line[$i][7],
                ]);
                $user->save();
            }

            return back()->with('status', 'Użytkownicy z pliku zostali dodani do bazy danych!');
        }
        catch(\Exception $exception){
            return back()->with('error', 'Z Twoim plikiem jest coś nie tak... Zapoznaj się z wymaganiami pliku!');
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_draw(StoreDrawRequest $request)
    {
        $amount = $request->amount;
        $faker = Faker::create('pl_PL');
        $colors = array('Piwne', 'Niebieskie', 'Zielone', 'Brązowe');

        for ($i=1; $i<=$amount; $i++) {
            $birth_date = $faker->dateTimeBetween('-60 years', '-28 years');
            $user = Client::create([
                'name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'city' => $faker->city,
                'street' => $faker->streetAddress,
                'birthday' => $birth_date->format('Y-m-d'),
                'eyes' => $colors[array_rand($colors)],
                'growth' => rand(160,199),
                'description' => $faker->realText(rand(20,60)),
            ]);
            $user->save();
        }

        return back()->with('status', 'Losowi użytkownicy zostali dodani do bazy!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $taskOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $taskOne)
    {
        $users = Client::truncate();
        return back()->with('status', 'Baza użytkowników została wyczyszczona!');

    }
}
