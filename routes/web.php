<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Tworzenie i niszczenie tabel
Route::get('/create_database', 'ClientController@create_database')->name('create_database');
Route::get('/delete_database', 'ClientController@delete_database')->name('delete_database');



Route::get('/', 'ClientController@index')->name('index');
Route::post('/', 'ClientController@index')->name('index_post');
Route::post('/store', 'ClientController@store')->name('store');
Route::post('/store_from_file', 'ClientController@store_from_file')->name('store_from_file');
Route::post('/strore_draw', 'ClientController@store_draw')->name('store_draw');
Route::delete('/delete', 'ClientController@destroy')->name('delete');

// Export csv
Route::get('/export_csv', 'ClientController@export_csv')->name('export_csv');


// Produkty
Route::prefix('products')->group(function () {
    Route::get('/', 'ProductController@index')->name('products');
    Route::post('/add', 'ProductController@store')->name('store_product');
    Route::delete('/delete', 'ProductController@destroy')->name('delete_products');
});


//// Lista zadań
//Route::get('/', function () {
//    return view('welcome');
//});
//
