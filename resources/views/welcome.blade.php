@extends('layouts')
@section('content')
    <div class="row">
        <div class="col mt-5">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Zadanie</th>
                    <th scope="col">Oddano</th>
                    <th scope="col">Stan</th>
                    <th scope="col">Operacja</th>
                    <th scope="col">Git</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td><a href="{{ route('zad1_index') }}" target="_blank">Zadanie 1</a></td>
                    <td>03.03.2019</td>
                    <td>Oddano</td>
                    <td>
                        <a href="{{ route('create_database') }}" target="_blank">Utwórz tabele</a> /
                        <a href="{{ route('delete_database') }}" target="_blank">Usuń tabele</a>
                    </td>
                    <td><a href="https://bitbucket.org/mm_mesiek_pl/aplikacje_baz_danych" target="_blank">Sprawdź</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection