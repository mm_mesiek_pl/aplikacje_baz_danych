@extends('layouts')
@section('content')
    <div class="row mt-5">
        <div class="col-12">


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
        </div>
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    Produkty
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            @if(!$products->count() <= 0)
                            <table class="table table-hove table-responsive-sm" style="font-size: .95rem;">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nazwa</th>
                                    <th scope="col">Cena</th>
                                    <th scope="col">Ilość</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <th scope="row">{{ $product->id }}</th>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->quantity }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                                <div class="alert alert-warning text-center" role="alert">
                                    <h4>Brak produktów w bazie!</h4>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col d-flex justify-content-end">
                            <a href="" class="btn btn-primary text-white mr-1 btn-sm" data-toggle="modal" data-target=".dodaj">Dodaj</a>
                            <a href="" class="btn btn-danger text-white btn-sm" data-toggle="modal" data-target=".czysc">Wyczyść produkty</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Dodaj -->
    <div id="addProduct" class="modal fade dodaj" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Dodawanie produktu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('store_product') }}">
                    <div class="modal-body">
                        @if ($errors->has('name','price','quantity') || $errors->has('price') || $errors->has('quantity') )
                            <div class="alert alert-danger">
                                <ul class="mb-0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @csrf
                        <div class="form-group">
                            <label for="name">Nazwa *</label>
                            <input type="text" class="form-control" id="name" placeholder="Walizka" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="price">Cena *</label>
                            <input type="text" class="form-control" id="price" placeholder="342" value="{{ old('price') }}" name="price">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Ilość *</label>
                            <input type="text" class="form-control" id="quantity" placeholder="97" name="quantity" value="{{ old('quantity') }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                        <input type="submit" class="btn btn-primary" value="Dodaj">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Czysc -->
    <div class="modal fade czysc" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Czy wyczyścić tabelę produktów?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('delete_products') }}" method="post">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-block" value="Wyczyść tabelę produktów!">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection