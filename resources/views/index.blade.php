@extends('layouts')
@section('content')
    <div class="row mt-5">
        <div class="col-12">


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
        </div>
        <div class="col-md-12 col-lg-3 mb-4 mb-lg-0">
            <div class="card">
                <div class="card-header">
                    Wyszukiwarka
                </div>
                <div class="card-body">
                    <form action="{{ route('index') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Szukana fraza</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="fraza" placeholder="Szukam..." value="{{ $fraza }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">W kolumnie</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="kolumna">
                                @foreach($columns as $key => $value)
                                    @if($key == $kolumna)
                                        <option selected value="{{ $key }}">{{ $value }}</option>
                                    @else
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="szukaj" class="btn btn-block btn-primary btn-sm" value="Szukaj">
                            <input type="submit" name="resetuj" class="btn btn-block btn-warning text-white btn-sm" value="Resetuj">
                        </div>
                    </form>

                    @if($count_clients != null)

                    <h5 class="text-center">Wyników: {{ $count_clients }}</h5>
                    @endif
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-header">
                    Wyszukaj z pliku
                </div>
                <div class="card-body">
                    <form action="{{ route('index') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="file_query">Dodaj plik</label>
                            <input type="file" class="form-control" id="file_query" name="file_query" data-toggle="tooltip"  data-placement="top" title="kolumna:fraza">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="szukaj_plik" class="btn btn-block btn-primary btn-sm" value="Szukaj">
                            <input type="submit" name="resetuj_plik" class="btn btn-block btn-warning text-white btn-sm" value="Resetuj">
                        </div>
                    </form>
                    <div class="form-group">
                        <button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="popover" title="Jak wyszukać za pomocą pliku" data-html="true" data-content="
                        <ul>
                            <li>Plik musi być w formacie .txt a jego kodowanie to UTF-8 (bez BOM)</li>
                            <li>Plik przyjmuje tylko jedną linię</li>
                            <li>Przykład struktury pliku: <strong>tabela:fraza</strong></li>
                            <li>Przykładowy plik do pobrania <a href='{{ url('query.txt') }}'>PLIK</a></li>
                            <li>Tabele muszą być w języku angielskim <i>(name, last_name, city, street, birthday, eyes, growth, description)</i></li>
                        </ul>">Sprawdź instrukcję <i class="fa fa-hand-o-left" aria-hidden="true"></i>
                        </button>
                    </div>
                    @if($count_file_clients != null)
                        <h5 class="text-center">Wyników: {{ $count_file_clients }}</h5>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-9">
            <div class="card">
                <div class="card-header">
                    Klienci
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            @if(!$clients->count() <= 0)
                            <table class="table table-hove table-responsive-sm" style="font-size: .95rem;">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Imię</th>
                                    <th scope="col">Nazwisko</th>
                                    <th scope="col">Miasto</th>
                                    <th scope="col">Ulica</th>
                                    <th scope="col">Urodziny</th>
                                    <th scope="col">Oczy</th>
                                    <th scope="col">Wzrost</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $client)
                                    <tr>
                                        <th scope="row">{{ $client->id }}</th>
                                        <td>{{ $client->name }}</td>
                                        <td>{{ $client->last_name }}</td>
                                        <td>{{ $client->city }}</td>
                                        <td>{{ $client->street }}</td>
                                        <td>{{ $client->birthday }}</td>
                                        <td>{{ $client->eyes }}</td>
                                        <td>{{ $client->growth }} cm</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                                <div class="alert alert-warning text-center" role="alert">
                                    <h4>Brak klientów w bazie!</h4>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        @if(Session::has('pharse'))
                        <div class="col d-flex justify-content-start">
                            <a href="{{ route('export_csv') }}" target="_blank" class="btn btn-sm btn-primary">Exportuj</a>
                        </div>
                        @endif
                        <div class="col d-flex justify-content-end">
                            <a href="" class="btn btn-primary text-white mr-1 btn-sm" data-toggle="modal" data-target=".dodaj">Dodaj</a>
                            <a href="" class="btn btn-primary text-white mr-1 btn-sm" data-toggle="modal" data-target=".dodaj-z-pliku">Dodaj z pliku</a>
                            <a href="" class="btn btn-primary text-white mr-1 btn-sm" data-toggle="modal" data-target=".wylosuj">Wylosuj</a>
                            <a href="" class="btn btn-danger text-white btn-sm" data-toggle="modal" data-target=".czysc">Wyczyść bazę</a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <div class="row mb-5">
        <div class="col d-flex justify-content-end">
            <div class="">

    </div>



    <!-- Dodaj -->
    <div id="addClient" class="modal fade dodaj" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Dodawanie użytkownika</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('store') }}">
                    <div class="modal-body">
                        @if ($errors->first('name','last_name','city','street','birthday','eyes','growth','description'))
                            <div class="alert alert-danger">
                                <ul class="mb-0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @csrf
                        <div class="form-group">
                            <label for="name">Imię *</label>
                            <input type="text" class="form-control" id="name" placeholder="Marek" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="last_name">Nazwisko *</label>
                            <input type="text" class="form-control" id="last_name" placeholder="Michalczyk" value="{{ old('last_name') }}" name="last_name">
                        </div>
                        <div class="form-group">
                            <label for="city">Miasto *</label>
                            <input type="text" class="form-control" id="city" placeholder="Kłobuck" name="city" value="{{ old('city') }}">
                        </div>
                        <div class="form-group">
                            <label for="street">Ulica *</label>
                            <input type="text" class="form-control" id="street" placeholder="Staszica 2" name="street" value="{{ old('street') }}">
                        </div>
                        <div class="form-group">
                            <label for="birthday">Urodziny *</label>
                            <input type="date" class="form-control" id="birthday" placeholder="10.09.1996" name="birthday" value="{{ old('birthday') }}">
                        </div>
                        <div class="form-group">
                            <label for="eyes">Oczy *</label>
                            <input type="text" class="form-control" id="eyes" placeholder="Piwne" name="eyes" value="{{ old('eyes') }}">
                        </div>
                        <div class="form-group">
                            <label for="growth">Wzrost *</label>
                            <input type="text" class="form-control" id="growth" placeholder="179" name="growth" value="{{ old('growth') }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Opis *</label>
                            <textarea class="form-control" id="description" rows="3" name="description">{{ old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                        <input type="submit" class="btn btn-primary" value="Dodaj">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Dodaj z pliku -->
    <div class="modal fade dodaj-z-pliku" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Dodawanie użytkownika z pliku</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{ route('store_from_file') }}" enctype="multipart/form-data">
                        <div class="modal-body">
                            @csrf
                            <div class="form-group">
                                <label for="file">Dodaj plik</label>
                                <input type="file" class="form-control" id="file" name="file">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="mb-0 font-weight-bold">Przykład lini w pliku</p>
                                    <code>Marek, Michalczyk, Kłobuck, Staszica 2, 2019-03-03, Brązowe, 123, Lorem ipsum...</code>
                                    <ul class="text-danger small mt-2">
                                        <li>Plik tekstowy musi być zapisany w UTF-8</li>
                                        <li>Każda linia to nowy użytkownik</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                            <input type="submit" class="btn btn-primary" value="Dodaj">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Wylosuj -->
    <div id="storeDraw" class="modal fade wylosuj" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Losowanie użytkownika</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{ route('store_draw') }}">
                        <div class="modal-body">
                            @if ($errors->first('amount'))
                                <div class="alert alert-danger">
                                    <ul class="mb-0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @csrf
                            <div class="form-group">
                                <label for="amount">Ilość użytkowników</label>
                                <input type="text" class="form-control" id="amount" placeholder="10" name="amount" value="{{ old('amount') }}">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                            <input type="submit" class="btn btn-primary" value="Losuj">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Wylosuj -->
    <div class="modal fade czysc" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Czy wyczyścić bazę?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('delete') }}" method="post">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-block" value="Wyczyść bazę danych!">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection